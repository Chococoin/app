module.exports.cron = {

  dailyFunds: {

    schedule: '0 0 * * *',

    onTick: async function () {
      var deactivateNow = Date.now();
      var activeUserTime = deactivateNow - sails.config.custom.maxInactivity;
      var users = await User.find({'active': true});

      async function creditUser (user) {
        if (user.lastTransaction >= activeUserTime) {
          await sails.helpers.payin(user.id, sails.config.custom.dailyAmount);
        }
        else{
          await User.updateOne(user.id).set({'active': false, 'deactivateDate': deactivateNow});
        }
      }

      users.forEach(creditUser);
    }

  },

  reactivateUserDailyFunds: {

    schedule: '0 1 * * *',

    onTick: async function () {
      var activeUserTime = Date.now() - sails.config.custom.maxInactivity;
      var reactivateUserTime = Date.now() - sails.config.custom.reactivateTime;
      var users = await User.find({'active': false}).where([{'lastTransaction':{'>=': activeUserTime}}, {'deactivateDate':{'<': reactivateUserTime}}]);

      async function reactivateUser (user) {
        await User.updateOne(user.id).set({'active': true});
      }

      users.forEach(reactivateUser);
    }

  },

  decreaseUserFunds: {

    schedule: '0 2 * * *',

    onTick: async function () {
      var decreaseNow = Date.now();
      var decreaseUserTime = decreaseNow - sails.config.custom.decreaseTime;
      var users = await User.find().where({'decreaseDate':{'<': decreaseUserTime}});

      async function decreaseUserFunds (user) {
        var decreaseAmount = (user.funds / 100) * sails.config.custom.decreasePercentage;

        if (decreaseAmount >= 0.01) {
          await sails.helpers.payout(user.id, decreaseAmount);
        }
        await User.updateOne(user.id).set({'decreaseDate': decreaseNow});
      }

      users.forEach(decreaseUserFunds);
    }

  },

};
