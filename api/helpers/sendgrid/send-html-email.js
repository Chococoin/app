module.exports = {


  friendlyName: 'Send template email',


  description: 'Send an email using a template.',


  extendedDescription: 'To ease testing and development, if the provided "to" email address ends in "@example.com", '+
    'then the email message will be written to the terminal instead of actually being sent.'+
    '(Thanks [@simonratner](https://github.com/simonratner)!)',


  inputs: {

    to: {
      description: 'The email address of the primary recipient.',
      extendedDescription: 'If this is any address ending in "@example.com", then don\'t actually deliver the message. '+
        'Instead, just log it to the console.',
      example: 'nola.thacker@example.com',
      required: true,
      isEmail: true,
    },

    subject: {
      type: 'string',
      description: 'The subject of the email.',
      example: 'Hello there.',
      defaultsTo: ''
    },

    from: {
      type: 'string',
      description: 'An override for the default "from" email that\'s been configured.',
      example: 'anne.martin@example.com',
      isEmail: true,
    },

    text: {
      type: 'string',
      description: 'Text mail'
    },

    html: {
      type: 'string',
      description: 'HTML mail'
    }

  },


  fn: async function({to, from, subject, text, html}) {

    const sgMail = require('@sendgrid/mail')
    sgMail.setApiKey(sails.config.custom.sendgridSecret)
    const msg = {
      to: to,
      from: from,
      subject: subject,
      text: text,
      html: html,
    }
    sgMail
      .send(msg)
      .then(() => {
        sails.log('Email sent')
      })
      .catch((error) => {
        sails.error(error)
      })

    // All done!
    return {

    };

  }

};
