module.exports = {

  friendlyName: 'Show date',

  description: 'Show date.',

  inputs: {
    date: {
      description: 'Show date.',
      // By declaring a numeric example, Sails will automatically respond with `res.badRequest`
      // if the `userId` parameter is not a number.
      type: 'number',
      // By making the `userId` parameter required, Sails will automatically respond with
      // `res.badRequest` if it's left out.
      required: true
    }
  },

  exits: {

    success: {
      description: '',
    },

  },

  fn: async function ({date}) {

    var date = new Date(date);
    var dd = date.getDate();
    var mm = date.getMonth()+1;
    var yyyy = date.getFullYear();

    if (dd<10) {
        dd='0'+dd;
    }

    if (mm<10) {
      mm='0'+mm;
    }

    date = dd+'/'+mm+'/'+yyyy;

    return {
      date: date
    };
  }
};
