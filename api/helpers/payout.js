module.exports = {

  friendlyName: 'Remove amount to funds',

  description: 'Remove amount to funds.',

  inputs: {
    userId: {
      description: 'The ID of the user to look up.',
      // By declaring a numeric example, Sails will automatically respond with `res.badRequest`
      // if the `userId` parameter is not a number.
      type: 'number',
      // By making the `userId` parameter required, Sails will automatically respond with
      // `res.badRequest` if it's left out.
      required: true
    },
    amount: {
      description: 'The amount to remove',

      type: 'number',

      required: true
    }
  },

  exits: {

    success: {
      description: 'The requesting socket is now subscribed to socket broadcasts about the logged-in user\'s session.',
    },

    negativeAmount: {
      description: 'Negative amount',
    },

    insufficientFunds: {
      description: 'Insufficient funds',
    },

  },

  fn: async function ({userId, amount}) {

    if(amount < 0) { throw 'negativeAmount'; }

    var user = await User.findOne(userId);

    if (!user) { throw 'notFound'; }

    var currentAmount = user.funds;
    var newAmount = Math.round((currentAmount - amount) * 100) / 100;

    if(amount > currentAmount) { throw 'insufficientFunds'; }

    await User.updateOne(user.id).set({'funds': newAmount});

    return {
      amount: user.amount
    };
  }
};
