module.exports = {


  friendlyName: 'Send value',


  description: 'Send value to user and create new transaction',


  extendedDescription:
`Create new transaction`,


  inputs: {

    email: {
      description: 'The email of the user to look up.',

      type: 'string',

      required: true
    },

    amount: {
      description: 'The amount to transfer',

      type: 'number',

      required: true
    },

    reason: {
      description: 'Reason of the transaction',

      type: 'string'
    },

  },


  exits: {

    success: {
      description: 'New user account was created successfully.'
    },

  },


  fn: async function ({email, amount, reason}) {

    var user = await User.findOne(this.req.session.userId);
    var payee = await User.findOne({'emailAddress': email});

    if(!user || !payee) {
      throw 'notFound';
    }

    await sails.helpers.payout(user.id, amount);

    await sails.helpers.payin(payee.id, amount);

    var transaction = await Transaction.create({'amount': amount, 'from': user.id, 'to': payee.id, 'reason': reason, 'lastTransaction': Date.now()}).fetch();

    return {
      transaction: transaction,
    }

  }

};
