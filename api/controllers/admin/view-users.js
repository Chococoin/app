module.exports = {


  friendlyName: 'View users',


  description: 'Show users.',

  inputs: {


  },


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/users',
      description: 'Display users.'
    },

  },


  fn: async function () {

    var users = await User.find().sort([{id:'ASC'}]);

    return {
      users: users
    };

  }


};
