module.exports = {


  friendlyName: 'View transactions',


  description: 'Show transactions.',

  inputs: {


  },


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/transactions',
      description: 'Display transactions.'
    },

  },


  fn: async function () {

    var transactions = await Transaction.find().populate('from').populate('to').sort([{id:'ASC'}]);

    return {
      transactions: transactions
    };

  }


};
