module.exports = {


  friendlyName: 'Search user(s)',


  description: 'Search user(s) by email',


  inputs: {

    emailAddress: {
      type: 'string',
      description: 'The new, unencrypted password.',
      required: true
    }

  },


  fn: async function ({emailAddress}) {

    var found = await User.findOne({emailAddress: emailAddress}).select(['id', 'fullName']);

    return {
      found,
    }

  }


};
