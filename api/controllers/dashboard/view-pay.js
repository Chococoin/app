module.exports = {


  friendlyName: 'View new transaction page',


  description: 'Display the new transaction page.',

  inputs: {

    email: {
      type: 'string'
    }

  },


  exits: {

    success: {
      viewTemplatePath: 'pages/dashboard/pay',
      description: 'Display the new transaction page for authenticated users.'
    },

  },


  fn: async function ({email}) {

    var user = this.req.me;

    return {
      name: user.fullName,
      funds: user.funds,
      toEmail: email
    };

  }


};
