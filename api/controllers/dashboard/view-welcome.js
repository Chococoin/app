module.exports = {


  friendlyName: 'View welcome page',


  description: 'Display the dashboard "Welcome" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/dashboard/welcome',
      description: 'Display the welcome page for authenticated users.'
    },

  },


  fn: async function () {

    var user = this.req.me;
    var foundTransactions = await Transaction.find().where({or: [{from: user.id}, {to: user.id}]}).limit(30).sort([{createdAt: 'DESC'}]).populate('from').populate('to');
    var transactions = [];
    var decreaseDate = await sails.helpers.showDate(user.decreaseDate);

    for (var i = 0; i < foundTransactions.length; i++){
      let t = foundTransactions[i];
      let type = t.from.id == user.id ? 'out' : 'in';
      let u = type == 'in' ? t.from : t.to;
      let createdAt = await sails.helpers.showDate(t.createdAt);

      transactions.push({
        date: createdAt.date,
        amount: t.amount,
        reason: t.reason,
        type: type,
        withUserEmail: u.emailAddress,
        withUserName: u.fullName
      });
    }

    return {
      name: user.fullName,
      funds: user.funds,
      decrease: decreaseDate.date,
      active: user.active,
      transactions: transactions
    };

  }


};
